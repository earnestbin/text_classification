#此函数实现了使用word2vec向量化文本,使用LDA分类，准确率高达84%
#并使用各种各样的分类器分类
import numpy as np 
import re 
#定义一个函数将文本读取到list中
def transfer(file,tag):
    neg= []
    neg_data=[]
    for line in open(file,'r',encoding='utf-8'):
        neg.append(line)
    regEx= re.compile('\\W*')
    for line in neg:
        result= regEx.split(line)
        result= [tok.lower() for tok in result if len(tok)>0]
        neg_data.append(result)
    if tag=='neg':
        labels= np.ones(len(neg))
    if tag=='pos':
        labels= np.zeros(len(neg))
    return neg_data,labels

#定义一个函数，将两类样本合并成以一个样本
def combine():
    neg_data, neg_labels= transfer(r'C:\Users\earnestbin\Desktop\data\neg.txt',tag='neg')
    pos_data, pos_labels= transfer(r'C:\Users\earnestbin\Desktop\data\pos.txt',tag='pos')
    Train_data=[]
    labels= []
    Train_data= neg_data+pos_data
    labels= np.hstack((neg_labels,pos_labels))
    return Train_data, labels 



#划分数据集，并使用word2vec训练
def word2vec():
    from sklearn.model_selection import train_test_split
    train_data, label= combine()
    x_train, x_test, y_train, y_test= train_test_split(train_data, label, test_size=0.33, random_state=0)
    from gensim.models.word2vec import Word2Vec
    model= Word2Vec(train_data, size=300, min_count=40, window=10,workers=4)
    
    def makeFeatureVec(words, model, num_features):
        # Function to average all of the word vectors in a given
        # paragraph
        #
        # Pre-initialize an empty numpy array (for speed)
        featureVec = np.zeros((num_features,),dtype="float32")
        #
        nwords = 0.
        # 
        # Index2word is a list that contains the names of the words in 
        # the model's vocabulary. Convert it to a set, for speed 
        index2word_set = set(model.wv.index2word)
        #
        # Loop over each word in the review and, if it is in the model's
        # vocaublary, add its feature vector to the total
        for word in words:
            if word in index2word_set: 
                nwords = nwords + 1.
                featureVec = np.add(featureVec,model[word])
        # 
        # Divide the result by the number of words to get the average
        featureVec = np.divide(featureVec,nwords)
        return featureVec


    def getAvgFeatureVecs(reviews, model, num_features):
        # Given a set of reviews (each one a list of words), calculate 
        # the average feature vector for each one and return a 2D numpy array 
        # 
        # Initialize a counter
        counter = 0
        # Preallocate a 2D numpy array, for speed
        reviewFeatureVecs = np.zeros((len(reviews),num_features),dtype="float32")
        # Loop through the reviews
        for review in reviews:
            if counter%1000 == 0:
                print("Review %d of %d" % (counter, len(reviews)))
            # Call the function (defined above) that makes average feature vectors
            reviewFeatureVecs[counter] = makeFeatureVec(review, model, num_features)
            counter = counter + 1
        return reviewFeatureVecs
    
    trainDataVVecs= getAvgFeatureVecs(x_train, model,300)
    testDataVVecs= getAvgFeatureVecs(x_test, model,300)
    return trainDataVVecs, testDataVVecs, y_train, y_test 

#分类器选择
def classifier(mol, xtrain, ytrain, xtest, ytest):
    if mol=='LDA':
        from sklearn.discriminant_analysis import LinearDiscriminantAnalysis as LDA
        clf=LDA()
        clf.fit(xtrain, ytrain)
        accuracy=clf.score(xtest, ytest)

    if mol=='logstic':
        from sklearn.linear_model import LogisticRegression
        clf=LogisticRegression()
        clf.fit(xtrain, ytrain)
        accuracy=clf.score(xtest, ytest)

    if mol=='knn':
        from sklearn.neighbors import KNeighborsClassifier
        clf=KNeighborsClassifier()
        clf.fit(xtrain, ytrain)
        accuracy=clf.score(xtest, ytest) 
    return accuracy